import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Dictionary dict = new Dictionary();

        String komenda;
        do {
            System.out.println("Podaj komende (dodaj / tlumacz / quit):");
            komenda = scanner.nextLine();


            String[] slowa = komenda.split(" ");
            switch (slowa[0]) {
                case "dodaj":
                    if (slowa.length < 3 || slowa.length > 4) {
                        System.out.println("Nie zrozumialem jakie tlumaczenie chcesz dodac...");
                        System.out.println("Podaj polskie slowo:");
                        String slowoPln = scanner.nextLine();
                        System.out.println("Podaj angielskie tlumaczenie:");
                        String slowoEng = scanner.nextLine();
                        dict.dodaj(slowoPln, slowoEng);
                    }
                    if (slowa.length == 3) {
                        dict.dodaj(slowa[1], slowa[2]);
                    }
                    if (slowa.length == 4) {
                        dict.dodaj(slowa[1], slowa[2], slowa[3]);
                    }
                    break;
                case "tlumacz":
                    System.out.println("po angielsku: " + dict.getEnglish(slowa[1]));
                    System.out.println("po hiszpansku: " + dict.getSpanish(slowa[1]));
                    break;
                case "quit":
                    break;
                default:
                    System.out.println("Nie rozpoznaje komendy");
            }
        } while (!komenda.equalsIgnoreCase("quit"));

    }
}
