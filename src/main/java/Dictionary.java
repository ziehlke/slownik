import java.util.*;

public class Dictionary {

    Map<String, String[]> dict = new HashMap<>();


    public void dodaj(String polish, String english) {
        String[] trans = new String[]{english};
        dict.put(polish, trans);
        System.out.println("dodano " + polish);
    }

    public void dodaj(String polish, String english, String spanish) {
        String[] trans = new String[]{english, spanish};
        dict.put(polish, trans);
        System.out.println("dodano " + polish);
    }

    public String getEnglish(String polish) {
        return dict.get(polish).length < 1 ? "-brak tlumaczenia-" : dict.get(polish)[0];
    }

    public String getSpanish(String polish) {
        return dict.get(polish).length < 2 ? "-brak tlumaczenia hiszpanskiego-" : dict.get(polish)[1];
    }

}